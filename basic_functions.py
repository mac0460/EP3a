import torch.nn.functional as F
import numpy as np
import torch
from util import randomize_in_place


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def graph1(a_np, b_np, c_np):
    """
    Computes the graph
        - x = a * c
        - y = a + b
        - f = x / y

    Computes also df/da using
        - Pytorchs's automatic differentiation (auto_grad)
        - user's implementation of the gradient (user_grad)

    :param a_np: input variable a
    :type a_np: np.ndarray(shape=(1,), dtype=float64)
    :param b_np: input variable b
    :type b_np: np.ndarray(shape=(1,), dtype=float64)
    :param c_np: input variable c
    :type c_np: np.ndarray(shape=(1,), dtype=float64)
    :return: f, auto_grad, user_grad
    :rtype: torch.DoubleTensor(shape=[1]),
            torch.DoubleTensor(shape=[1]),
            numpy.float64
    """
    # YOUR CODE HERE:

    a = torch.from_numpy(a_np)
    b = torch.from_numpy(b_np)
    c = torch.from_numpy(c_np)
    a.requires_grad = True
    x = a * c
    y = a + b
    f = x / y
    f.backward()
    auto_grad = a.grad
    user_grad = (b_np * c_np) / (a_np + b_np)**2
    
    # END YOUR CODE
    return f, auto_grad, user_grad


def graph2(W_np, x_np, b_np):
    """
    Computes the graph
        - u = Wx + b
        - g = sigmoid(u)
        - f = sum(g)

    Computes also df/dW using
        - pytorchs's automatic differentiation (auto_grad)
        - user's own manual differentiation (user_grad)

    F.sigmoid may be useful here

    :param W_np: input variable W
    :type W_np: np.ndarray(shape=(d,d), dtype=float64)
    :param x_np: input variable x
    :type x_np: np.ndarray(shape=(d,1), dtype=float64)
    :param b_np: input variable b
    :type b_np: np.ndarray(shape=(d,1), dtype=float64)
    :return: f, auto_grad, user_grad
    :rtype: torch.DoubleTensor(shape=[1]),
            torch.DoubleTensor(shape=[d, d]),
            np.ndarray(shape=(d,d), dtype=float64)
    """
    # YOUR CODE HERE:

    # Pytorch
    W = torch.from_numpy(W_np)
    W.requires_grad = True
    x = torch.from_numpy(x_np)
    b = torch.from_numpy(b_np)
    u = torch.matmul(W, x) + b
    g = F.sigmoid(u)
    f = sum(g)
    f.backward()
    auto_grad = W.grad
    
    # Numpy
    g_np = g.detach().numpy()
    
    df_dg = np.ones((1, np.size(g_np)))
    dg_du = np.diagflat(g_np * (1 - g_np))
    
    d, n = W_np.shape
    du_dW = np.zeros((d, d, n))
    for k in range(0, n):
        for i in range(0, d):
            du_dW[i][i][k] = x_np[k]
    
    user_grad = df_dg.dot(dg_du.dot(du_dW))

    # END YOUR CODE
    return f, auto_grad, user_grad


def SGD_with_momentum(X,
                      y,
                      inital_w,
                      iterations,
                      batch_size,
                      learning_rate,
                      momentum):
    """
    Performs batch gradient descent optimization using momentum.

    :param X: design matrix
    :type X: np.ndarray(shape=(N, d))
    :param y: regression targets
    :type y: np.ndarray(shape=(N, 1))
    :param inital_w: initial weights
    :type inital_w: np.array(shape=(d, 1))
    :param iterations: number of iterations
    :type iterations: int
    :param batch_size: size of the minibatch
    :type batch_size: int
    :param learning_rate: learning rate
    :type learning_rate: float
    :param momentum: accelerate parameter
    :type momentum: float
    :return: weights, weights history, cost history
    :rtype: np.array(shape=(d, 1)), list, list
    """
    # YOUR CODE HERE:

    def compute_cost(X, y, w):
        y_hat = torch.matmul(X, w)
        return torch.mean((y_hat - y)**2)
    
    N, d = X.shape
    w = torch.from_numpy(inital_w)
    w.requires_grad = True
    z = torch.zeros(d, 1).type(torch.DoubleTensor)
    
    X_bat = torch.from_numpy(X[0:batch_size])
    y_bat = torch.from_numpy(y[0:batch_size])
    
    J = compute_cost(X_bat, y_bat, w)
    J.backward()
    w_grad = w.grad
    
    
    cost_history = [J]
    weights_history = [w]
    
    for i in range(iterations):
        
        randomize_in_place(X, y, i)
        X_bat = torch.from_numpy(X[0:batch_size])
        y_bat = torch.from_numpy(y[0:batch_size])
        
        w_grad.zero_()
        J = compute_cost(X_bat, y_bat, w)
        J.backward()
        
        z = z * momentum + w_grad
        w = w - learning_rate * z
        
        cost_history.append(J)
        weights_history.append(w)
        
    w_np = w.detach().numpy()

    # END YOUR CODE

    return w_np, weights_history, cost_history
